const entities = {
	buscarCliente: {
		contrato: '',
		nombreCliente: '',
		existeCliente: false,
		cliente: {},
		resultadoBusqueda: [],
	},
	modificarFechaPago: {},
	pagareContrato: {
		pagare: {},
		imprimirPagare: '',
		activarPagare: '',
	},
	pcd: {
		datosCliente: {},
		historialProtecciones: [],
		pagosAtrasados: [],
	},
	asignarCajaCajero: {
		listCajasCerradas: [],
		labelInfoCaja: {},
		listaProspectos: [],
	},
	monitorCentral: {
		listaOficinas: [],
		initRefresh: false,
		timeRefresh: 0,
	},

	trasladoValores: {
		listaCajasSeguridad: [],
		labelInfoCaja: {},
		listaTraslados: [],
		trasladoRealizado: {},
	},
	reportes: {
		catalogoOperaciones: [],
		catalogoTipoDisposicion: [],
		catalogoTipoFecha: [],
		catalogoEstatusTarjeta: [],
		catalogoTipoProducto: [],
		listaCajasActivas: [],
		listaCajas: [],
		reporte: '',
	},
};

export default entities;
