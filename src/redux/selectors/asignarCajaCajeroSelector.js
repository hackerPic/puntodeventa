import { createSelector } from 'reselect';

const asignarCajaCajeroListCajasCerradasData = state =>
	state.entities.asignarCajaCajero.listCajasCerradas;
const asignarCajaCajeroLabelInfoCaja = state => state.entities.asignarCajaCajero.labelInfoCaja;
const asignarCajaCajerosListaProspecto = state => state.entities.asignarCajaCajero.listaProspectos;

// Reselect functions
export const getListCajasCerradasDataState = createSelector(
	[asignarCajaCajeroListCajasCerradasData],
	listCajasCerradas => listCajasCerradas
);

export const getAsignaCajaCajeroLabelInfoCajaState = createSelector(
	[asignarCajaCajeroLabelInfoCaja],
	labelInfoCaja => labelInfoCaja
);

export const getAsignaCajaCajeroListaTrasladoState = createSelector(
	[asignarCajaCajerosListaProspecto],
	listaProspectos => listaProspectos
);
