import { createSelector } from 'reselect';

// Selectors
const consultaOficinas = state => state.entities.monitorCentral.listaOficinas;
const getTimeRefresh = state => state.entities.monitorCentral.timeRefresh;
const getInitRefresh = state => state.entities.monitorCentral.initRefresh;

export const getOficinasMonitorCentralState = createSelector(
	[consultaOficinas],
	listaOficinas => listaOficinas
);

export const getTimeRefreshState = createSelector(
	[getTimeRefresh],
	timeRefresh => timeRefresh
);
export const getInitRefreshState = createSelector(
	[getInitRefresh],
	initRefresh => initRefresh
);
