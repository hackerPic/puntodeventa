import { createSelector } from 'reselect';

// Selectors
const getConsultaOficina = state => state.entities.reportes.catalogoOficinas;
const getConsultaCatalogoOperaciones = state => state.entities.reportes.catalogoOperaciones;
const getConsultaCatalogoTipoDisposicion = state => state.entities.reportes.catalogoTipoDisposicion;
const getConsultaCatalogoTipoProducto = state => state.entities.reportes.catalogoTipoProducto;
const getConsultaCatalogoTipoFecha = state => state.entities.reportes.catalogoTipoFecha;
const getConsultaCatalogoEstatusTarjeta = state => state.entities.reportes.catalogoEstatusTarjeta;
const getConsultaCajasActivas = state => state.entities.reportes.listaCajasActivas;
const getConsultaCaja = state => state.entities.reportes.listaCajas;
const getConsultaReporte = state => state.entities.reportes.reporte;

// Reselect functions
export const getConsultaOficinaReporteState = createSelector(
	[getConsultaOficina],
	catalogoOficinas => catalogoOficinas
);
export const getConsultaCatalogoOperacionesState = createSelector(
	[getConsultaCatalogoOperaciones],
	catalogoOperaciones => catalogoOperaciones
);

export const getConsultaCatalogoTipoDisposicionState = createSelector(
	[getConsultaCatalogoTipoDisposicion],
	catalogoTipoDisposicion => catalogoTipoDisposicion
);

export const getConsultaCatalogoTipoFechaState = createSelector(
	[getConsultaCatalogoTipoFecha],
	catalogoTipoFecha => catalogoTipoFecha
);

export const getConsultaCatalogoEstatusTarjetaState = createSelector(
	[getConsultaCatalogoEstatusTarjeta],
	catalogoEstatusTarjeta => catalogoEstatusTarjeta
);

export const getConsultaCatalogoTipoProductoState = createSelector(
	[getConsultaCatalogoTipoProducto],
	catalogoTipoProducto => catalogoTipoProducto
);

export const getConsultaCajaActivasReporteState = createSelector(
	[getConsultaCajasActivas],
	listaCajasActivas => listaCajasActivas
);

export const getConsultaCajaReporteState = createSelector(
	[getConsultaCaja],
	listaCajas => listaCajas
);

export const getConsultaOperacionReporteState = createSelector(
	[getConsultaReporte],
	reporte => reporte
);
