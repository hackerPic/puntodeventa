import { createSelector } from 'reselect';

const consultaDatosCliente = state => state.entities.pcd.datosCliente;
const consultaHistorial = state => state.entities.pcd.historialProtecciones;
const consultaPagosAtrasados = state => state.entities.pcd.pagosAtrasados;
const consultaStatusButton = state => state.entities.pcd.statusButton;

export const getConsultaClienteState = createSelector(
	[consultaDatosCliente],
	datosCliente => datosCliente
);

export const getHistorialState = createSelector(
	[consultaHistorial],
	historialProtecciones => historialProtecciones
);

export const getPagosAtrasadosState = createSelector(
	[consultaPagosAtrasados],
	pagosAtrasados => pagosAtrasados
);

export const getStatusButtonState = createSelector(
	[consultaStatusButton],
	statusButton => statusButton
);
