import { createSelector } from 'reselect';

// Selectors

const trasladoValoresListaCajasSeguridad = state =>
	state.entities.trasladoValores.listaCajasSeguridad;
const trasladoValoresLabelInfoCaja = state => state.entities.trasladoValores.labelInfoCaja;
const trasladoValoresListaTraslado = state => state.entities.trasladoValores.listaTraslados;
const trasladoValoresTrasladoRealizado = state => state.entities.trasladoValores.trasladoRealizado;

// Reselect functions
export const getTrasladoValoresListaCajasSeguridadState = createSelector(
	[trasladoValoresListaCajasSeguridad],
	listaCajasSeguridad => listaCajasSeguridad
);

export const getTrasladoValoresLabelInfoCajaState = createSelector(
	[trasladoValoresLabelInfoCaja],
	labelInfoCaja => labelInfoCaja
);

export const getTrasladoValoresListaTrasladoState = createSelector(
	[trasladoValoresListaTraslado],
	listaTraslados => listaTraslados
);

export const getTrasladoValoresTrasladoRealizadoState = createSelector(
	[trasladoValoresTrasladoRealizado],
	trasladoRealizado => trasladoRealizado
);
