import { createSelector } from 'reselect';

// Selectors
const consultaPagare = state => state.entities.pagareContrato.pagare;

const buscarClienteContrato = state => state.entities.buscarCliente.contrato;
const buscarClienteCliente = state => state.entities.buscarCliente.cliente;
const buscarClienteListaClientes = state => state.entities.buscarCliente.listaClientes;

export const getConsultaPagareState = createSelector(
	[consultaPagare],
	pagare => pagare
);
export const getBuscarClienteContratoState = createSelector(
	[buscarClienteContrato],
	contrato => contrato
);
export const getBuscarClienteClienteState = createSelector(
	[buscarClienteCliente],
	cliente => cliente
);

export const getBscarClienteListaClientesState = createSelector(
	[buscarClienteListaClientes],
	listaClientes => listaClientes
);
