import { createSelector } from 'reselect';
import store from '../store/index';

// Selectors
const getHeaderRequest = state => state.app.headerRequest;
const getUserProfile = state => state.app.userProfile;
const getUserProfileSucursal = state => state.app.userProfile.sucursal;
const getPersona = state => state.app.userProfile.noPersona;
const getBackOfficeReady = state => state.app.backOfficeReady;
const getOficina = state => state.app.oficina;
const getOficinas = state => state.app.oficinas;
export const getAppSelector = () => store.app;

// Reselect functions
export const getPersonaState = createSelector(
	[getPersona],
	noPersona => noPersona
);
export const getUserProfileSucursalState = createSelector(
	[getUserProfileSucursal],
	sucursal => sucursal
);
export const getHeaderRequestState = createSelector(
	[getHeaderRequest],
	headerRequest => headerRequest
);
export const getUserProfileState = createSelector(
	[getUserProfile],
	userProfile => userProfile
);
export const getBackOfficeReadyState = createSelector(
	[getBackOfficeReady],
	backOfficeReady => backOfficeReady
);
export const getOficinaState = createSelector(
	[getOficina],
	oficina => oficina
);
export const getOficinasState = createSelector(
	[getOficinas],
	oficinas => oficinas
);
