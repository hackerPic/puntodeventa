import { SET_ACTION } from 'actions/getter.actions';

export const PAGARES = '[PAGARES]';
export const IMPRIMIR_PAGARE = `${PAGARES} IMPRIMIR`;
export const ACTIVAR_PAGARE = `${PAGARES} ACTIVAR`;

const entityPagares = {
	epic: 'pagareContrato',
	module: 'pagare',
};

const entityImprimePagare = {
	epic: 'pagareContrato',
	module: 'imprimirPagare',
};

const entityActivaPagare = {
	epic: 'pagareContrato',
	module: 'activarPagare',
};

export const setDataPagares = ({ payload }) => ({
	type: `${PAGARES} ${SET_ACTION}`,
	payload,
	entity: entityPagares,
});

export const setDataImprimePagare = ({ payload }) => ({
	type: `${PAGARES} ${SET_ACTION}`,
	payload,
	entity: entityImprimePagare,
});

export const setDataActivaPagare = ({ payload }) => ({
	type: `${PAGARES} ${SET_ACTION}`,
	payload,
	entity: entityActivaPagare,
});
