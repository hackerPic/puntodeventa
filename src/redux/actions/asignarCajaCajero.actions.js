import { SET_ACTION } from 'actions/getter.actions';

export const ASIGNAR_CAJA_CAJERO = '[ASIGNAR_CAJA_CAJERO]';
export const CONSULTAR_CAJAS = `${ASIGNAR_CAJA_CAJERO} CONSULTAR_CAJAS`;
export const CONSULTAR_PROSPECTOS = `${ASIGNAR_CAJA_CAJERO} CONSULTAR_PROSPECTOS`;
export const REGISTRAR_ASIGNACION_CAJERO = `${ASIGNAR_CAJA_CAJERO} REGISTRAR`;

const entityCajas = {
	epic: 'asignarCajaCajero',
	module: 'listCajasCerradas',
};

const entityInformacionCaja = {
	epic: 'asignarCajaCajero',
	module: 'labelInfoCaja',
};

const entityProspectos = {
	epic: 'asignarCajaCajero',
	module: 'listaProspectos',
};

export const setListaCajasSeguridad = ({ payload }) => ({
	type: `${CONSULTAR_CAJAS} ${SET_ACTION}`,
	payload,
	entity: entityCajas,
});

export const setLabelInfo = ({ payload }) => ({
	type: `${CONSULTAR_PROSPECTOS} ${SET_ACTION}`,
	payload,
	entity: entityInformacionCaja,
});

export const setListaProspectos = ({ payload }) => ({
	type: `${CONSULTAR_PROSPECTOS} ${SET_ACTION}`,
	payload,
	entity: entityProspectos,
});
