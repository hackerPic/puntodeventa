import { SET_ACTION } from 'actions/getter.actions';

export const REPORTES = '[REPORTES]';
export const SET_TIPO_OPERACION = `${REPORTES} OPERACION`;
export const REPORTES_INIT = `${REPORTES} INIT`;
export const GET_CATALOGO_OFICINAS = `${REPORTES} OBTENER_CATALOGO_OFICINAS`;
export const GET_CATALOGO_REPORTES = `${REPORTES} OBTENER_CATALOGO_REPORTES`;
export const CONSULTAR_CAJAS = `${REPORTES} CONSULTAR_CAJAS`;
export const CONSULTAR_CAJAS_ACTIVAS = `${REPORTES} CONSULTAR_CAJAS_ACTIVAS`;
export const CONSULTAR_REPORTE_OPERACION = `${REPORTES} CONSULTAR_REPORTE_OPERACION`;
export const CONSULTAR_CATALOGOS_REPORTE_OPERACION = `${REPORTES} CONSULTAR_CATALOGO_REPORTE_OPERACION`;
export const SET_CATALOGO_OFICINAS = `${REPORTES} SET_CATALOGO_OFICINAS`;
export const SET_CATALOGO_OPERACIONES = `${REPORTES} SET_CATALOGO_OPERACIONES`;
export const SET_CATALOGO_TIPO_DISPOSICION = `${REPORTES} SET_CATALOGO_TIPO_DISPOSICION`;
export const SET_CATALOGO_TIPO_FECHA = `${REPORTES} SET_CATALOGO_TIPO_FECHA`;
export const SET_CATALOGO_ESTATUS_TARJETA = `${REPORTES} SET_CATALOGO_ESTATUS_TARJETA`;
export const SET_CATALOGO_TIPO_PRODUCTO = `${REPORTES} SET_CATALOGO_TIPO_PRODUCTO`;
export const SET_CAJAS_ACTIVAS = `${REPORTES} SET_CAJAS_ACTIVAS`;

const entityCatalogoOficinas = {
	epic: 'reportes',
	module: 'catalogoOficinas',
};
const entityCatalogoOperaciones = {
	epic: 'reportes',
	module: 'catalogoOperaciones',
};
const entityCatalogoTipoDisposicion = {
	epic: 'reportes',
	module: 'catalogoTipoDisposicion',
};

const entityCatalogoTipoFecha = {
	epic: 'reportes',
	module: 'catalogoTipoFecha',
};

const entityCatalogoEstatusTarjeta = {
	epic: 'reportes',
	module: 'catalogoEstatusTarjeta',
};

const entityCatalogoTipoProducto = {
	epic: 'reportes',
	module: 'catalogoTipoProducto',
};
const entityCajasActivas = {
	epic: 'reportes',
	module: 'listaCajasActivas',
};

const entityCajas = {
	epic: 'reportes',
	module: 'listaCajas',
};
const entityReporteTipoOperacion = {
	epic: 'reportes',
	module: 'reporte',
};

export const setCatalogoOficinas = ({ payload }) => ({
	type: `${SET_CATALOGO_OFICINAS} ${SET_ACTION}`,
	payload,
	entity: entityCatalogoOficinas,
});
export const setCatalogoOperaciones = ({ payload }) => ({
	type: `${SET_CATALOGO_OPERACIONES} ${SET_ACTION}`,
	payload,
	entity: entityCatalogoOperaciones,
});
export const setCatalogoTipoDisposicion = ({ payload }) => ({
	type: `${SET_CATALOGO_TIPO_DISPOSICION} ${SET_ACTION}`,
	payload,
	entity: entityCatalogoTipoDisposicion,
});

export const setCatalogoTipoFecha = ({ payload }) => ({
	type: `${SET_CATALOGO_TIPO_FECHA} ${SET_ACTION}`,
	payload,
	entity: entityCatalogoTipoFecha,
});

export const setCatalogoEstatusTarjeta = ({ payload }) => ({
	type: `${SET_CATALOGO_ESTATUS_TARJETA} ${SET_ACTION}`,
	payload,
	entity: entityCatalogoEstatusTarjeta,
});

export const setCatalogoTipoProducto = ({ payload }) => ({
	type: `${SET_CATALOGO_TIPO_PRODUCTO} ${SET_ACTION}`,
	payload,
	entity: entityCatalogoTipoProducto,
});

export const setCajasActivas = ({ payload }) => ({
	type: `${SET_CAJAS_ACTIVAS} ${SET_ACTION}`,
	payload,
	entity: entityCajasActivas,
});

export const setCajas = ({ payload }) => ({
	type: `${CONSULTAR_CAJAS} ${SET_ACTION}`,
	payload,
	entity: entityCajas,
});
export const setReporteOperacion = ({ payload }) => ({
	type: `${CONSULTAR_REPORTE_OPERACION} ${SET_ACTION}`,
	payload,
	entity: entityReporteTipoOperacion,
});
