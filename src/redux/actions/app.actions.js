/* Action types */
export const BACKOFFICE = '[BACKOFFICE]';
export const BACKOFFICE_INIT = `${BACKOFFICE} INIT`;
export const IS_READY = `${BACKOFFICE} IS_READY`;
export const SCANNER = `${BACKOFFICE} SCANNING`;

// Url params
export const GET_PARAMS_URL = `${BACKOFFICE} GET_PARAMS_URL`;
export const SET_PARAMS_URL_REQUEST = `${BACKOFFICE} SET_PARAMS_URL_REQUEST`;
// User
export const GET_USER_PROFILE = `${BACKOFFICE} GET_USER_PROFILE`;
export const SET_USER_PROFILE = `${BACKOFFICE} SET_USER_PROFILE`;
// Sucursales
export const GET_SUCURSAL = `${BACKOFFICE} GET_SUCURSAL`;
export const GET_SUCURSALES = `${BACKOFFICE} GET_SUCURSALES`;
export const SET_SUCURSAL = `${BACKOFFICE} SET_SUCURSAL`;
export const SET_SUCURSALES = `${BACKOFFICE} SET_SUCURSALES`;

/* Action creators */
export const getParamsUrl = () => ({
	type: GET_PARAMS_URL,
});
export const setHeaderRequest = headerRequest => ({
	type: SET_PARAMS_URL_REQUEST,
	payload: headerRequest,
});
export const setUserProfile = userProfile => ({
	type: SET_USER_PROFILE,
	payload: userProfile,
});
export const setBackOfficeReady = ready => ({
	type: IS_READY,
	payload: ready,
});
export const setSucursal = oficina => ({
	type: SET_SUCURSAL,
	payload: oficina,
});
export const setSucursales = oficinas => ({
	type: SET_SUCURSALES,
	payload: oficinas,
});
