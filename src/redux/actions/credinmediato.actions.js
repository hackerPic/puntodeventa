import { SET_ACTION } from 'actions/getter.actions';

export const CREDINMEDIATO = '[CREDINMEDIATO]';
export const APLICAR_CREDINMEDIATO = `${CREDINMEDIATO} APLICAR`;

export const setNoContrato = () => ({
	type: `${APLICAR_CREDINMEDIATO} ${SET_ACTION}`,
});
