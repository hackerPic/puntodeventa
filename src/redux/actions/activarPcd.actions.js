import { SET_ACTION } from 'actions/getter.actions';

export const PCD = '[PCD]';
export const ACTIVAR_PCD = `${PCD} ACTIVAR`;
export const DESACTIVAR_PCD = `${PCD} DESACTIVAR`;
export const DATOS_CLIENTE = `${PCD} SET_DATOS_CLIENTE`;
export const HISTORIAL_PROTECCIONES = `${PCD} HISTORIAL_PROTECCIONES`;
export const PAGOS_ATRASADOS = `${PCD} PAGOS_ATRASADOS`;
export const ESTATUS_BUTTON = `${PCD} ESTATUS_BUTTON`;

const entityDatosCliente = {
	epic: 'pcd',
	module: 'datosCliente',
};
const entityDatoshistorialProtecciones = {
	epic: 'pcd',
	module: 'historialProtecciones',
};
const entityDatospagosAtrasados = {
	epic: 'pcd',
	module: 'pagosAtrasados',
};

const entityStatusButton = {
	epic: 'pcd',
	module: 'statusButton',
};

export const setDatosCliente = ({ payload }) => ({
	type: `${DATOS_CLIENTE} ${SET_ACTION}`,
	payload,
	entity: entityDatosCliente,
});

export const setHistorialProtecciones = ({ payload }) => ({
	type: `${HISTORIAL_PROTECCIONES} ${SET_ACTION}`,
	payload,
	entity: entityDatoshistorialProtecciones,
});

export const setPagosAtrasados = ({ payload }) => ({
	type: `${PAGOS_ATRASADOS} ${SET_ACTION}`,
	payload,
	entity: entityDatospagosAtrasados,
});

export const setStatusButton = ({ payload }) => ({
	type: `${ESTATUS_BUTTON} ${SET_ACTION}`,
	payload,
	entity: entityStatusButton,
});
