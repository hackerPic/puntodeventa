import { SET_ACTION } from 'actions/getter.actions';

export const TRASLADO_VALORES = '[TRASLADO VALORES]';
export const TRASLADO_VALORES_INIT = `${TRASLADO_VALORES} init`;
export const CONSULTAR_CAJAS = `${TRASLADO_VALORES} CONSULTA_CAJAS`;
export const CONSULTAR_TRASLADO = `${TRASLADO_VALORES} CONSULTAR_TRASLADO`;
export const REGISTRAR_TRASLADO = `${TRASLADO_VALORES} REGISTRAR`;
export const CANCELAR_TRASLADO = `${TRASLADO_VALORES} CANCELAR`;
export const OBTIENE_PDF_TRASLADO = `${TRASLADO_VALORES} OBTENER_PDF`;

const entityCajas = {
	epic: 'trasladoValores',
	module: 'listaCajasSeguridad',
};

const entityLabelCaja = {
	epic: 'trasladoValores',
	module: 'labelInfoCaja',
};

const entityTraslados = {
	epic: 'trasladoValores',
	module: 'listaTraslados',
};

export const setListaCajasSeguridad = ({ payload }) => ({
	type: `${CONSULTAR_CAJAS} ${SET_ACTION}`,
	payload,
	entity: entityCajas,
});

export const setLabelInfo = ({ payload }) => ({
	type: `${CONSULTAR_TRASLADO} ${SET_ACTION}`,
	payload,
	entity: entityLabelCaja,
});

export const setListaTraslados = ({ payload }) => ({
	type: `${CONSULTAR_TRASLADO} ${SET_ACTION}`,
	payload,
	entity: entityTraslados,
});
