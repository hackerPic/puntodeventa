import { SET_ACTION } from 'actions/getter.actions';

export const MONITOR_CENTRAL = '[MONITOR CENTRAL]';
export const BUSQUEDA_OFICINAS = `${MONITOR_CENTRAL} búsqueda de oficinas`;
export const TIME_CONSULTAR_OFICINAS = `${MONITOR_CENTRAL} set time update`;
export const INIT_AUTO_UPDATE_CONSULTAR_OFICINAS = `${MONITOR_CENTRAL} set init auto update`;

const entityOficinas = {
	epic: 'monitorCentral',
	module: 'listaOficinas',
};

const entityTimeRefresh = {
	epic: 'monitorCentral',
	module: 'timeRefresh',
};
const entityInitRefresh = {
	epic: 'monitorCentral',
	module: 'timeRefresh',
};

export const setDataOficinas = ({ payload }) => ({
	type: `${MONITOR_CENTRAL} ${SET_ACTION}`,
	payload,
	entity: entityOficinas,
});
export const setTimeRefresh = ({ payload }) => ({
	type: `${TIME_CONSULTAR_OFICINAS} ${SET_ACTION}`,
	payload,
	entity: entityTimeRefresh,
});
export const setInitRefresh = ({ payload }) => ({
	type: `${INIT_AUTO_UPDATE_CONSULTAR_OFICINAS} ${SET_ACTION}`,
	payload,
	entity: entityInitRefresh,
});
