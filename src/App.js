import React, { Component, Fragment } from 'react';
import { BrowserRouter as Router, Route, Switch, Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { getBackOfficeReadyState } from 'selectors/appSelector';
import DynamicImport from 'constants/DynamicImport';
import DynamicContainer from 'util/DynamicContainer/DynamicContainer';
import './App.css';

const GalileoProgress = DynamicImport('components/GalileoProgress/GalileoProgress');
const GalileoNotifications = DynamicImport('components/GalileoNotifications/GalileoNotifications');
const GalileoSnackBar = DynamicImport('components/GalileoSnackBar/GalileoSnackBar');
const BreadCrumb = DynamicImport('components/BreadCrumb/BreadCrumb');

@connect(store => ({
	backOfficeReady: getBackOfficeReadyState(store),
}))
class App extends Component {
	render() {
		return (
			<Router>
				<Fragment>
					<Link style={{ margin: '0 5px' }} to="/backoffice/modificar-fecha-pago">
						Modificar fecha de pago
					</Link>
					<Link style={{ margin: '0 5px' }} to="/backoffice/pagares">
						Pagarés
					</Link>
					<Link style={{ margin: '0 5px' }} to="/backoffice/activar-pcd">
						Activar PCD
					</Link>
					<Link style={{ margin: '0 5px' }} to="/backoffice/reportes">
						Reportes
					</Link>
					<Link style={{ margin: '0 5px' }} to="/backoffice/extension-de-pagos">
						Extension de pagos
					</Link>
					<Link style={{ margin: '0 5px' }} to="/backoffice/traslado-de-valores">
						Traslado de valores
					</Link>
					<Link style={{ margin: '0 5px' }} to="/backoffice/asignar-caja-cajero">
						Asignar caja-cajero
					</Link>
					<Link style={{ margin: '0 5px' }} to="/backoffice/activar-credinmediato">
						Activar credinmediato
					</Link>
					<Link style={{ margin: '0 5px' }} to="/backoffice/registrar-firma">
						Registrar firma
					</Link>
					<Link style={{ margin: '0 5px' }} to="/backoffice/monitor-de-efectivo-central">
						Monitor central
					</Link>
					<div className="wrapper-galileo">
						<BreadCrumb {...this.props} />
						<Switch>
							<Route
								exact
								strict
								path="/:page?/:subpage?"
								render={({ match, history }) => (
									<DynamicContainer {...match} {...history} {...this.props} />
								)}
							/>
						</Switch>
					</div>
					<GalileoProgress />
					<GalileoNotifications />
					<GalileoSnackBar />
				</Fragment>
			</Router>
		);
	}
}

export default App;
