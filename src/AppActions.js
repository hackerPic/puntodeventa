/* eslint-disable no-use-before-define */
import {
	fetchUsuarioActivo,
	fetchMapeoEmpresas,
	fetchUsuarioFacultades,
	fetchApi,
} from 'api/fetchApi';
import {
	CONSULTAR_USUARIO_ACTIVO,
	CONSULTAR_MAPEO_EMPRESAS,
	CONSULTAR_USUARIO_FACULTADES,
	CONSULTA_OFICINAS_POST,
} from 'constants/Endpoints';
import {
	BACKOFFICE_INIT,
	GET_SUCURSALES,
	getParamsUrl,
	setHeaderRequest,
	setBackOfficeReady,
	setUserProfile,
	setSucursal,
	setSucursales,
} from 'actions/app.actions';
import { createBrowserHistory } from 'history';
import handleProgress from 'util/HandleProgress';
import handleNotification from 'util/HandleNotification';
import { hideNotification } from 'actions/notification.actions';
import {
	getParamsObject,
	parseSucursal,
	parseSucursales,
	parseExitstMenu,
	handleCompareFaculties,
	hasFacultyBackoffice,
} from './AppUtil';

const handleCancel = dispatch => {
	dispatch(hideNotification(BACKOFFICE_INIT));
};
const handleResetInit = (dispatch, push, feature, profile, usuarioActivo) => {
	handleCancel(dispatch, feature);
	initConfiguration(dispatch, push, profile, usuarioActivo);
};
const handleExitView = (dispatch, push, feature) => {
	handleCancel(dispatch, feature);
	push('/');
};
const handleErrorBackOffice = (dispatch, push, feature, profile, usuarioActivo) => {
	handleProgress(dispatch, {
		show: false,
		feature,
	});
	handleNotification(dispatch, {
		type: 'error',
		content: 'Hubo un error al cargar los servicios de Backoffice.',
		children: '¿Qué desea hacer?',
		okButtonTitle: 'Reintentar',
		onOk: () => handleResetInit(dispatch, push, feature, profile, usuarioActivo),
		onCancel: () => handleExitView(dispatch, push, feature),
		feature,
		autoFocus: true,
	});
};
const handleErrorFaculties = (dispatch, complementaryText, push, profile, noPersona, pathname) => {
	handleProgress(dispatch, {
		show: false,
		feature: '[USUARIO FACULTADES]',
	});
	handleNotification(dispatch, {
		type: 'error',
		content: 'Hubo un error con el usuario que ha iniciado sesión',
		children: `El usuario no tiene asignada la facultad para operar "${complementaryText}". Consulte al administrador de sistemas e inicie sesión nuevamente.`,
		okButtonTitle: 'Cerrar',
		onOk: () => handleExitView(dispatch, push, '[USUARIO FACULTADES]'),
		feature: '[USUARIO FACULTADES]',
		autoFocus: true,
	});
};
const requestOffices = async (dispatch, push, profile, usuarioActivo) => {
	const { fechaOperacion, claveEmpresa, idOperador, idOficina } = profile;
	const sucursalesRequest = {
		headerRequest: {
			fechaOperacion,
			idOficina,
			idOperador,
			claveEmpresa,
		},
	};
	const consultaSucursalesQuery = {
		data: sucursalesRequest,
		feature: GET_SUCURSALES,
		endpoint: CONSULTA_OFICINAS_POST,
		message: 'Iniciando...',
		notification: false,
		push,
	};
	const response = await dispatch(fetchApi(consultaSucursalesQuery));
	if (response.code === 200) {
		return response;
	}
	if (response.code !== 200) {
		handleErrorBackOffice(dispatch, push, BACKOFFICE_INIT, profile, usuarioActivo);
	}
};
const requestOffice = async (dispatch, { claveSucursal, descripcionSucursal }) => {
	return parseSucursal(claveSucursal, descripcionSucursal);
};
const userProfileRequest = (profile, usuarioActivo) => {
	return {
		nombreCajero: `${usuarioActivo.nombre} ${usuarioActivo.apellidoPaterno} ${usuarioActivo.apellidoMaterno}`,
		claveUsuario: usuarioActivo.claveUsuario,
		empresa: usuarioActivo.descripcionEmpresa,
		sucursal: usuarioActivo.descripcionSucursal,
		idOperador: profile.idOperador,
		noPersona: usuarioActivo.noPersona,
	};
};
const initConfiguration = async (dispatch, push, profile, usuarioActivo, isInit = false) => {
	if (!isInit) {
		handleProgress(dispatch, {
			feature: BACKOFFICE_INIT,
		});
	}
	const dataUserProfileRequest = await userProfileRequest(profile, usuarioActivo);
	const getDataOffice = await requestOffice(dispatch, usuarioActivo);
	const getDataOffices = await requestOffices(dispatch, push, profile, usuarioActivo);

	try {
		await dispatch(setUserProfile(dataUserProfileRequest));
		await dispatch(setHeaderRequest(profile));
		await dispatch(setSucursal(getDataOffice));
		await dispatch(setSucursales(parseSucursales(getDataOffices.lstOficinas)));
		await dispatch(setBackOfficeReady(true));
	} catch (error) {
		handleProgress(dispatch, {
			show: false,
			feature: BACKOFFICE_INIT,
		});
		handleErrorBackOffice(dispatch, push, BACKOFFICE_INIT, profile, usuarioActivo);
	}
};
const handleExistsMenu = path => {
	const pathname = path.substring(0).split('/')[2];
	return parseExitstMenu(pathname);
};
const handleBusinessMapping = async dispatch => {
	const dataMapeoEmpresas = {
		empresaSirh: '19',
	};
	const queryMapeoEmpresas = {
		data: dataMapeoEmpresas,
		feature: '[MAPEO EMPRESAS]',
		endpoint: CONSULTAR_MAPEO_EMPRESAS,
	};
	return dispatch(fetchMapeoEmpresas(queryMapeoEmpresas));
};
const handleUserFaculties = async (dispatch, push, profile, noPersona, pathname) => {
	let claveEmpresa = '';
	const dataUsuarioActivo = {
		noPersona,
	};
	const queryUsuarioActivo = {
		data: dataUsuarioActivo,
		feature: '[USUARIO ACTIVO]',
		endpoint: CONSULTAR_USUARIO_ACTIVO,
	};
	const usuarioActivo = await dispatch(fetchUsuarioActivo(queryUsuarioActivo));
	claveEmpresa = +usuarioActivo.claveEmpresa;

	if (claveEmpresa === 19) {
		claveEmpresa = await handleBusinessMapping(dispatch);
	}
	const dataUsuarioFacultades = {
		idPersona: usuarioActivo.noPersona,
		claveEmpresa: usuarioActivo.claveEmpresa,
	};
	const queryUsuarioFacultades = {
		data: dataUsuarioFacultades,
		feature: '[USUARIO FACULTADES]',
		endpoint: CONSULTAR_USUARIO_FACULTADES,
		notification: false,
	};
	const usuarioFacultades = await dispatch(fetchUsuarioFacultades(queryUsuarioFacultades));
	const { codigoFacultad, secondaryText } = await handleExistsMenu(pathname);

	if (!codigoFacultad) {
		handleErrorFaculties(dispatch, secondaryText, push);
		return;
	}
	if (hasFacultyBackoffice(usuarioFacultades).length === 0) {
		handleProgress(dispatch, {
			show: false,
			feature: 'USUARIO FACULTADES',
		});
		handleNotification(dispatch, {
			type: 'error',
			content: 'Hubo un error con el usuario que ha iniciado sesión',
			children:
				'El usuario no tiene la facultad principal de Backoffice. Consulte al administrador de sistemas e inicie sesión nuevamente.',
			okButtonTitle: 'Cerrar',
			onOk: () => handleExitView(dispatch, push, 'USUARIO FACULTADES'),
			feature: 'USUARIO FACULTADES',
			autoFocus: true,
		});
		// eslint-disable-next-line no-console
		console.error('Usuario no tiene la facultad principal de Backoffice');
		return;
	}

	const existsFaculties = handleCompareFaculties(codigoFacultad, usuarioFacultades);

	if (existsFaculties.length === 0) {
		handleErrorFaculties(dispatch, secondaryText, push);
		return;
	}
	initConfiguration(dispatch, push, profile, usuarioActivo);
};
const initBackOffice = async (dispatch, push, backOfficeReady = false) => {
	const { location } = document;
	const history = createBrowserHistory();
	history.push(
		`?claveEmpresa=${process.env.EMPRESA}&cveUsuario=${process.env.USUARIO}&oficina=${process.env.OFICINA}`
	);
	if (!location.search) {
		handleProgress(dispatch, {
			show: false,
			feature: BACKOFFICE_INIT,
		});
		handleNotification(dispatch, {
			type: 'error',
			content: 'Hubo un error al iniciar Operaciones.',
			children:
				'El módulo de Operaciones no tiene los parámetros necesarios iniciar. Consulte al administrador de sistemas e inicie sesión nuevamente.',
			okButtonTitle: 'Cerrar',
			onOk: () => handleExitView(dispatch, push, BACKOFFICE_INIT),
			feature: BACKOFFICE_INIT,
			autoFocus: true,
		});
		return;
	}

	if (backOfficeReady) return;
	dispatch(getParamsUrl());
	const parsedUrl = new URL(location);
	const noPersona = parsedUrl.searchParams.get('cveUsuario');
	if (!noPersona) {
		handleProgress(dispatch, {
			show: false,
			feature: BACKOFFICE_INIT,
		});
		handleNotification(dispatch, {
			type: 'error',
			content: 'Hubo un error al iniciar.',
			children:
				'La app no tiene asignado el parámetro de persona. Intente iniciando sesión nuevamente.',
			okButtonTitle: 'Cerrar',
			onOk: () => handleExitView(dispatch, push, BACKOFFICE_INIT),
			feature: BACKOFFICE_INIT,
			autoFocus: true,
		});
		return;
	}
	const profile = getParamsObject(location);
	handleUserFaculties(dispatch, push, profile, noPersona, location.pathname);
};

export default initBackOffice;
