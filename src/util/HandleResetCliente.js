import handleResetForm from 'util/HandleResetForm';
import handleResetEntity from 'util/HandleResetEntity';

const handleResetCliente = dispatch => {
	handleResetEntity(dispatch, 'buscarCliente', 'contrato', '');
	handleResetEntity(dispatch, 'buscarCliente', 'nombreCliente', '');
	handleResetEntity(dispatch, 'buscarCliente', 'cliente', {});
	handleResetEntity(dispatch, 'buscarCliente', 'resultadoBusqueda', []);
	handleResetEntity(dispatch, 'buscarCliente', 'existeCliente', false);
	handleResetForm(dispatch, 'BUSCAR_CLIENTE');
};

export default handleResetCliente;
