import React from 'react';
import DynamicImport from 'constants/DynamicImport';

const DynamicReport = props => {
	let Component = null;
	const { idReporte } = props;

	const componentPath = `containers/Reportes/TiposReporte/${idReporte}`;
	Component = DynamicImport(componentPath);
	return <Component {...props} />;
};
export default DynamicReport;
