const getAmountBarColor = ({ nivelMonto }) => {
	let background = '#3d8947';
	if (nivelMonto === 2) {
		background = '#f5a623';
	}
	if (nivelMonto === 1) {
		background = '#d0021b';
	}
	return background;
};

const updateEffectiveBar = nivelMonto => {
	const background = getAmountBarColor(nivelMonto);
	return { background };
};

export default updateEffectiveBar;
