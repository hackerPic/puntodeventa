/* -------CONSULTA MAPEO EMPRESAS---- */
export const CONSULTAR_MAPEO_EMPRESAS = '/cxf/seguridad/autorizacion/consultarMapeoEmpresas';
/* -------CONSULTAR FACULTADES------- */
export const CONSULTAR_USUARIO_EMAIL = '/cxf/seguridad/autorizacion/consultarUsuarioEmail';
export const CONSULTAR_USUARIO_ACTIVO = '/cxf/empleados/rest/buscarActivos';
export const CONSULTAR_USUARIO_FACULTADES =
	'/cxf/seguridad/autorizacion/consultarFacultadesActivasUsuarioGlobales';
/* -------AUTORIZA------- */
export const AUTORIZA_PERSONA_FACULTAD = '/cxf/seguridad/autorizacion/autorizaPersonaFacultad';

/* -------CONSULTAR OFICINAS------- */
export const CONSULTA_OFICINAS_POST = '/AdminEfectivoWSRest/rest/consultasRest/consultaOficina';
/* MODIFICAR FECHA PAGO */
export const CONSULTA_FECHA_PAGO_POST =
	'/AdminEfectivoWSRest/rest/operacionesRest/consultarFechaPago';
export const MODIFICA_FECHA_PAGO_POST =
	'/AdminEfectivoWSRest/rest/operacionesRest/modificarFechaPago';

/* PAGARES */
export const CONSULTA_PAGARES_CREDINMEDIATO_POST =
	'/AdminEfectivoWSRest/rest/consultasRest/consultaPagareCredinmediato';
export const IMPRIME_PAGARE_CREDINMEDIATO_POST =
	'/AdminEfectivoWSRest/rest/operacionesRest/imprimirPagareCredinmediato';
export const ACTIVA_PAGARE_CREDINMEDIATO_POST =
	'/AdminEfectivoWSRest/rest/operacionesRest/activarPagareCredinmediato';
/* PCD */
export const CONSULTA_CONTRATOS_PCD = '/AdminCarteraWSRest/rest/consultasRest/obtenerPCDContrato';
export const ACTIVAR_PCD_CONTRATO = '/AdminCarteraWSRest/rest/operacionesRest/activarPCDContrato';
export const INACTIVAR_PCD_CONTRATO =
	'/AdminCarteraWSRest/rest/operacionesRest/inactivarPCDContrato/';
/* REGISTRO FIRMA */
export const REGISTRAR_FIRMA_POST = '/AdminEfectivoWSRest/rest/consultasRest/registraClienteFirma/';
export const CONSULTA_CONTRATO_FIRMA_POST =
	'/AdminEfectivoWSRest/rest/consultasRest/consultarClienteFirma/';

/* CREDINMEDIATO */
export const CONSULTA_CLIENTES_NOMBRE_POST =
	'/AdminCarteraWSRest/rest/consultasRest/consultarClientes';
export const CONSULTA_CLIENTES_TELEFONO_POST =
	'/AdminCarteraWSRest/rest/consultasRest/consultarClientes';
export const CONSULTA_CLIENTE_CONTRATO_POST =
	'/AdminCarteraWSRest/rest/consultasRest/obtenerConfirmacionLCR';
export const REGISTRAR_CREDINMEDIATO_POST =
	'/IntegradorWSRest/rest/operacionesCajaRest/activaCredinmediatoCaja';

/* ASIGNA CAJA CAJERO */
export const CONSULTA_INFORMACION_ASIGNA_CAJA_POST =
	'/AdminEfectivoWSRest/rest/consultasRest/consultaCajerosCajasNoAbiertas';

export const REGISTRA_ASIGNA_CAJA_CAJERO_POST =
	'/AdminEfectivoWSRest/rest/operacionesRest/asignarCajaACajero';

/* MONITOR CENTRAL */
export const CONSULTA_OFICINAS_MONITOR_CENTRAL_POST =
	'/AdminEfectivoWSRest/rest/consultasRest/monitorEfectivoCentral';
/* TRASLADO VALORES */
export const CONSULTA_CAJAS_SEGURIDAD_POST =
	'/AdminEfectivoWSRest/rest/consultasRest/obtenerCatCajaSeguridad';
export const CONSULTA_TRASLADOS_CAJAS_SEGURIDAD_POST =
	'/AdminEfectivoWSRest/rest/consultasRest/obtenerDatosCajaSeguridad/';
export const REGISTRA_TRASLADO_VALORES_POST =
	'/AdminEfectivoWSRest/rest/operacionesRest/ejecutaTraslado';
export const CANCELA_TRASLADO_VALORES_POST =
	'/AdminEfectivoWSRest/rest/operacionesRest/cancelaTraslado';

/* REPORTES */
export const CONSULTA_OFICINAS_REPORTE_POST =
	'/AdminEfectivoWSRest/rest/consultasRest/consultaOficina';
export const CONSULTA_CAJAS_REPORTE_POST =
	'/AdminEfectivoWSRest/rest/consultasRest/obtenerCatCajas';
export const CONSULTA_CAJAS_ACTIVAS_REPORTE_POST =
	'/AdminEfectivoWSRest/rest/consultasRest/obtenerCatCajas';
export const CONSULTA_CATALOGOS_REPORTE_POST =
	'/AdminEfectivoWSRest/rest/consultasRest/consultaCatalogos';
export const CONSULTA_REPORTE_AUDITORIA_CAJA_POST =
	'/AdminEfectivoWSRest/rest/reportesPdf/auditoriaCajas';
export const CONSULTA_REPORTE_DISPOSICIONES_CAJA_POST =
	'/AdminEfectivoWSRest/rest/reportesPdf/reporteDisposiciones';
export const CONSULTA_REPORTE_CAJA_SEGURIDAD_POST =
	'/AdminEfectivoWSRest/rest/reportesPdf/reporteCajaSeguridad';
export const CONSULTA_REPORTE_TRASLADO_VALORES_CAJA_POST =
	'/AdminEfectivoWSRest/rest/reportesPdf/reporteTrasladoValores';
export const CONSULTA_REPORTE_TRASPASO_ENTRE_CAJAS_POST =
	'/AdminEfectivoWSRest/rest/reportesPdf/reporteTraspasoEfectivoCajas';
export const CONSULTA_REPORTE_FALTANTES_CAJA_POST =
	'/AdminEfectivoWSRest/rest/reportesPdf/reporteFaltantes';
export const CONSULTA_REPORTE_ACTIVACION_CREDINMEDIATO_POST =
	'/AdminEfectivoWSRest/rest/reportesPdf/activarCredInmediato';
export const CONSULTA_REPORTE_TARJETAS_CAJEROATM_POST =
	'/AdminEfectivoWSRest/rest/reportesPdf/reporteTarjetasCajeroATM';
export const CONSULTA_REPORTE_CONTRATOS_SIN_FIRMA_POST =
	'/AdminEfectivoWSRest/rest/reportesPdf/reporteRegistroFirmaContrato';

export const CONSULTA_EXTENSION_PAGOS =
	'/AdminCarteraWSRest/rest/consultasRest/consultaExtensionPagos';
export const APLICAR_EXTENSION_PAGOS =
	'/IntegradorWSRest/rest/operacionesCajaRest/aplicarExtensionPago';
