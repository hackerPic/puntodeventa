export default {
	successLoadingServices: 'Servicios cargados correctamente',
	errorLoadingServices: 'Ocurrió un error al cargar los servicios',
	errorSearchContract: 'Ocurrió un error al realizar la busqueda del contrato',
	successOperation: 'Operación aplicada correctamente',
	errorOperation: 'Ocurrió un error al aplicar la operación',
	successApi: 'Operación procesada correctamente',
	errorApi: 'Ocurrió un error al procesar la operación',
};
