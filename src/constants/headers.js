const headers = [
	{
		label: 'Contrato',
		key: 'contrato',
	},
	{
		label: 'Nombre',
		key: 'nombre',
	},
	{
		label: 'RFC',
		key: 'rfc',
	},
	{
		label: 'Dirección',
		key: 'direccion',
	},
	{
		label: 'Fecha contrato',
		key: 'fechaContrato',
		type: 'date',
	},
];

export default headers;
