export default [
	{
		id: 'm1',
		codigoFacultad: 'BACKOFAC001',
		primaryText: 'Backoffice',
		secondaryText: 'Modificar fecha de pago',
		componentName: 'ModificarFechaPago',
		parentUrl: '/backoffice',
		url: '/modificar-fecha-pago',
		path: 'modificar-fecha-pago',
	},
	{
		id: 'm2',
		codigoFacultad: 'BACKOFAC005',
		primaryText: 'Backoffice',
		secondaryText: 'Activar credinmediato',
		componentName: 'ActivarCredinmediato',
		parentUrl: '/backoffice',
		url: '/activar-credinmediato',
		path: 'activar-credinmediato',
	},
	{
		id: 'm3',
		codigoFacultad: 'BACKOFAC003',
		primaryText: 'Backoffice',
		secondaryText: 'Pagarés',
		componentName: 'Pagares',
		parentUrl: '/backoffice',
		url: '/pagares',
		path: 'pagares',
	},
	{
		id: 'm4',
		codigoFacultad: 'BACKOFAC002',
		primaryText: 'Backoffice',
		secondaryText: 'Activar protección contra desempleo',
		componentName: 'ActivarPcd',
		parentUrl: '/backoffice',
		url: '/activar-pcd',
		path: 'activar-pcd',
	},
	{
		id: 'm5',
		codigoFacultad: 'BACKOFAC010',
		primaryText: 'Backoffice',
		secondaryText: 'Asignar caja-cajero',
		componentName: 'AsignarCajaCajero',
		parentUrl: '/backoffice',
		url: '/asignar-caja-cajero',
		path: 'asignar-caja-cajero',
	},
	{
		id: 'm6',
		codigoFacultad: 'BACKOFAC011',
		primaryText: 'Backoffice',
		secondaryText: 'Monitor de efectivo central',
		componentName: 'MonitorEfectivoCentral',
		parentUrl: '/backoffice',
		url: '/monitor-de-efectivo-central',
		path: 'monitor-de-efectivo-central',
	},
	{
		id: 'm7',
		codigoFacultad: 'BACKOFAC004',
		primaryText: 'Backoffice',
		secondaryText: 'Registrar firma',
		componentName: 'RegistrarFirma',
		parentUrl: '/backoffice',
		url: '/registrar-firma',
		path: 'registrar-firma',
	},

	{
		id: 'm8',
		codigoFacultad: 'BACKOFAC008',
		primaryText: 'Backoffice',
		secondaryText: 'Traslado de valores',
		componentName: 'TrasladoValores',
		parentUrl: '/backoffice',
		url: '/traslado-de-valores',
		path: 'traslado-de-valores',
	},

	{
		id: 'm9',
		codigoFacultad: 'BACKOFAC009',
		primaryText: 'Backoffice',
		secondaryText: 'Reportes',
		componentName: 'Reportes',
		parentUrl: '/backoffice',
		url: '/reportes',
		path: 'reportes',
	},
	{
		id: 'm8',
		codigoFacultad: 'BACKOFAC007',
		primaryText: 'Backoffice',
		secondaryText: 'Extension de pagos',
		componentName: 'ExtensionPagos',
		parentUrl: '/backoffice',
		url: '/extension-de-pagos',
		path: 'extension-de-pagos',
	},
];
